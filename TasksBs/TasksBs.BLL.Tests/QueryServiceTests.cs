using System;
using System.Collections.Generic;
using System.Linq;
using TasksBs.BLL.Interfaces;
using TasksBs.BLL.Services;
using TasksBs.DAL.Entities;
using TasksBs.TestsCommon;
using Xunit;

namespace TasksBs.BLL.Tests
{
    public class QueryServiceTests : DbServicesTest
    {
        private readonly IQueryService _queryService;
        public QueryServiceTests() : base("QueryDb")
        {
            _queryService = new QueryService(_mapper, _context);
        }

        [Fact]
        public async System.Threading.Tasks.Task ExecuteQueryOne_UserWithTwoTasksInProject_ReturnsProjectWithTasksCount()
        {
            var user = await _context.AddUser();
            var team = await _context.AddTeam(members: new List<User>(new[] { user }));
            var project = await _context.AddProject(user.Id, team.Id);
            await _context.AddTask(user.Id, project.Id);
            await _context.AddTask(user.Id, project.Id);
            await _context.SaveChangesAsync();

            var result = await _queryService.ExecuteQueryOne(user.Id);

            Assert.Single(result);
            Assert.Equal(2, result.FirstOrDefault().Value);
        }

        [Fact]
        public async System.Threading.Tasks.Task ExecuteQueryTwo_UserWithTwoTasksInProjectWithNameSmallerThan45_ReturnsTwoTasksForUser()
        {
            var team = await _context.AddTeam();
            var user = await _context.AddUser(teamId: team.Id);
            var project = await _context.AddProject(user.Id, team.Id);
            await _context.AddTask(user.Id, project.Id, name: "t1");
            await _context.AddTask(user.Id, project.Id, name: "t2");
            await _context.AddTask(user.Id, project.Id, name: "12345678901234567890123456789012345678901234567890");
            await _context.SaveChangesAsync();

            var result = await _queryService.ExecuteQueryTwo(user.Id);

            Assert.True(result.Count() == 2);
            Assert.DoesNotContain(result, t => t.Name.Length > 45);
            Assert.DoesNotContain(result, t => t.Performer.Id != user.Id);
        }

        [Fact]
        public async System.Threading.Tasks.Task ExecuteQueryThree_UserWithTwoFinishedTasksInCurrentYear_ReturnsTwoFinishedTasksForUser()
        {
            var team = await _context.AddTeam();
            var user = await _context.AddUser(teamId: team.Id);
            var project = await _context.AddProject(user.Id, team.Id);
            await _context.AddTask(user.Id, project.Id, name: "t1", finishedAt: DateTime.Now.AddYears(-2));
            await _context.AddTask(user.Id, project.Id, name: "t1");
            await _context.AddTask(user.Id, project.Id, name: "t2", finishedAt: DateTime.Now);
            await _context.AddTask(user.Id, project.Id, name: "t3", finishedAt: DateTime.Now);
            await _context.SaveChangesAsync();

            var result = await _queryService.ExecuteQueryThree(user.Id);

            Assert.True(result.Count() == 2);
            Assert.Collection(result, t => Assert.Equal("t2", t.Value), t => Assert.Equal("t3", t.Value));
        }

        [Fact]
        public async System.Threading.Tasks.Task ExecuteQueryFour_OneTeamWithUsersOlderThan10Years_ReturnsOneTeamWithUsersSortedByDescending()
        {
            var team1 = await _context.AddTeam(name: "Team1");
            var team2 = await _context.AddTeam(name: "Team2");
            var user1 = await _context.AddUser(teamId: team1.Id, birthDate: DateTime.Now.AddYears(-15));
            var user2 = await _context.AddUser(teamId: team1.Id, birthDate: DateTime.Now.AddYears(-15));
            var user3 = await _context.AddUser(teamId: team2.Id, birthDate: DateTime.Now.AddYears(-15));
            var user4 = await _context.AddUser(teamId: team2.Id, birthDate: DateTime.Now.AddYears(-5));
            var project1 = await _context.AddProject(user1.Id, team1.Id);
            var project2 = await _context.AddProject(user1.Id, team2.Id);
            await _context.AddTask(user1.Id, project1.Id);
            await _context.AddTask(user2.Id, project1.Id);
            await _context.AddTask(user3.Id, project2.Id);
            await _context.AddTask(user4.Id, project2.Id);
            await _context.SaveChangesAsync();

            var result = await _queryService.ExecuteQueryFour();

            Assert.Single(result);
            var orderedList = result.First().Value.OrderByDescending(t => t.BirthDay).ToList();
            Assert.True(orderedList.SequenceEqual(result.First().Value));
            Assert.DoesNotContain(result.First().Value, t => t.BirthDay > DateTime.Now.AddYears(-10));
        }

        [Fact]
        public async System.Threading.Tasks.Task ExecuteQueryFive_TwoUsersWithTwoTasks_ReturnsTwoUsersOrderedByFirstNameAndTasksOrderedByNameLength()
        {
            var team = await _context.AddTeam();
            var user1 = await _context.AddUser(teamId: team.Id);
            var user2 = await _context.AddUser(teamId: team.Id);
            var project = await _context.AddProject(user1.Id, team.Id);
            await _context.AddTask(user1.Id, project.Id, name: "t1�����");
            await _context.AddTask(user1.Id, project.Id, name: "t223");
            await _context.AddTask(user2.Id, project.Id, name: "t4�");
            await _context.AddTask(user2.Id, project.Id, name: "t3��");
            await _context.SaveChangesAsync();

            var result = await _queryService.ExecuteQueryFive();

            var orderedListUsers = result.OrderBy(t => t.Key.FirstName).ToList();
            Assert.True(orderedListUsers.SequenceEqual(result));
            foreach (var user in result)
            {
                var orderedTasks = user.Value.OrderByDescending(t => t.Name.Length).ToList();
                Assert.True(orderedTasks.SequenceEqual(user.Value));
            }
            Assert.True(result.Count() == 2);
        }

        [Fact]
        public async System.Threading.Tasks.Task ExecuteQuerySix_UsersWithTasksAndUnfinishedTasks_ReturnsUserByIdWithLastProjectAndTasksAndWithLongestTask()
        {
            var team = await _context.AddTeam();
            var user1 = await _context.AddUser(teamId: team.Id);
            var user2 = await _context.AddUser(teamId: team.Id);
            var project1 = await _context.AddProject(user1.Id, team.Id, createdAt: DateTime.Now);
            var project2 = await _context.AddProject(user1.Id, team.Id, createdAt: DateTime.Now.AddDays(-5));
            await _context.AddTask(user1.Id, project1.Id, createdAt: DateTime.Now, finishedAt: DateTime.Now.AddDays(3));
            await _context.AddTask(user2.Id, project2.Id, createdAt: DateTime.Now.AddYears(-5), finishedAt: DateTime.Now);
            await _context.AddTask(user2.Id, project2.Id, createdAt: DateTime.Now);
            var longestTask = await _context.AddTask(user1.Id, project2.Id, createdAt: DateTime.Now, finishedAt: DateTime.Now.AddDays(10));
            await _context.SaveChangesAsync();

            var result = await _queryService.ExecuteQuerySix(user1.Id);

            Assert.Equal(project1.Id, result.LastProject.Id);
            Assert.Equal(1, result.LastProjectTasksCount);
            Assert.Equal(0, result.UnfinishedTasksCount);
            Assert.Equal(user1.Id, result.User.Id);
            Assert.Equal(longestTask.Id, result.LongestTask.Id);
        }

        [Fact]
        public async System.Threading.Tasks.Task ExecuteQuerySix_UsersWithTasksAndUnfinishedTasksWhenOneTaskWithNoFinishedDate_ReturnsUserByIdWithLastProjectAndTasksAndWithLongestTask()
        {
            var team = await _context.AddTeam();
            var user1 = await _context.AddUser(teamId: team.Id);
            var user2 = await _context.AddUser(teamId: team.Id);
            var project1 = await _context.AddProject(user1.Id, team.Id, createdAt: DateTime.Now);
            var project2 = await _context.AddProject(user1.Id, team.Id, createdAt: DateTime.Now.AddDays(-5));
            await _context.AddTask(user1.Id, project1.Id, createdAt: DateTime.Now, finishedAt: DateTime.Now.AddDays(15));
            await _context.AddTask(user2.Id, project2.Id, createdAt: DateTime.Now.AddYears(-5), finishedAt: DateTime.Now);
            await _context.AddTask(user2.Id, project2.Id, createdAt: DateTime.Now);
            var longestTask = await _context.AddTask(user1.Id, project2.Id, createdAt: DateTime.Now.AddDays(-25));
            await _context.SaveChangesAsync();

            var result = await _queryService.ExecuteQuerySix(user1.Id);

            Assert.Equal(project1.Id, result.LastProject.Id);
            Assert.Equal(1, result.LastProjectTasksCount);
            Assert.Equal(1, result.UnfinishedTasksCount);
            Assert.Equal(user1.Id, result.User.Id);
            Assert.Equal(longestTask.Id, result.LongestTask.Id);
        }

        [Fact]
        public async System.Threading.Tasks.Task ExecuteQuerySeven_TwoProjectsWithTasks_ReturnsProjectsAndTaskWithLongestDescAndTaskWithShortestNameWhereProjectDescriptionLengthMoreThan20OrTasksCountSmallerThan3()
        {
            var team = await _context.AddTeam();
            var user1 = await _context.AddUser(teamId: team.Id);
            var user2 = await _context.AddUser(teamId: team.Id);
            var project1 = await _context.AddProject(user1.Id, team.Id);
            var project2 = await _context.AddProject(user1.Id, team.Id);
            await _context.AddTask(user1.Id, project1.Id, description: "shortDescription", name: "longTaskNameeeeee");
            await _context.AddTask(user2.Id, project2.Id, description: "t1", name: "n1");
            await _context.AddTask(user2.Id, project2.Id, description: "t2", name: "n2");
            await _context.AddTask(user2.Id, project2.Id, description: "t3", name: "n3");
            var taskToCheck = await _context.AddTask(user1.Id, project1.Id, description: "longestDescription", name: "shortestName");
            await _context.SaveChangesAsync();

            var result = await _queryService.ExecuteQuerySeven();

            Assert.True(result.Count() == 2);
            Assert.Equal(taskToCheck.Id, result.First().LongestTask.Id);
            Assert.Equal(taskToCheck.Id, result.First().ShortestTask.Id);
            Assert.Collection(result, p => Assert.True(p.UsersCountInTeam == 2), p => Assert.True(p.UsersCountInTeam == null));
        }
    }
}
