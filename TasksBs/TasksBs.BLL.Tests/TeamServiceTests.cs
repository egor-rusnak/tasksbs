﻿using Microsoft.EntityFrameworkCore;
using System;
using TasksBs.BLL.Interfaces;
using TasksBs.BLL.Services;
using TasksBs.TestsCommon;
using Xunit;

namespace TasksBs.BLL.Tests
{
    public class TeamServiceTests : DbServicesTest
    {
        private readonly ITeamService _teamService;
        public TeamServiceTests() : base("teamDb")
        {
            _teamService = new TeamService(_context, _mapper);
        }

        [Fact]
        public async System.Threading.Tasks.Task AddUserToTeam_AddedUserToTeam()
        {
            var team = await _context.AddTeam();
            var user = await _context.AddUser();
            await _context.SaveChangesAsync();

            await _teamService.AddUserToTeam(team.Id, user.Id);

            Assert.True((await _context.Users.FirstOrDefaultAsync(u => u.Id == user.Id)).TeamId == team.Id);
        }

        [Fact]
        public async System.Threading.Tasks.Task AddUserToTeam_UserAlreadyHaveTeam_ThrowsArgumentException()
        {
            var team = await _context.AddTeam();
            var team2 = await _context.AddTeam();
            var user = await _context.AddUser(teamId: team2.Id);
            await _context.SaveChangesAsync();

            await Assert.ThrowsAsync<ArgumentException>(async () => await _teamService.AddUserToTeam(team.Id, user.Id));
        }
    }
}
