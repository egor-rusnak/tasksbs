﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TasksBs.DAL.Context;
using TasksBs.DAL.Entities;
using Task = TasksBs.DAL.Entities.Task;

namespace TasksBs.TestsCommon
{
    public static class ProjectDbExtensions
    {
        public static async Task<User> AddUser(this ProjectDbContext _context,
            int id = 0,
            string firstName = "FNAME",
            string lastName = "LNAME",
            string email = "EMAIL",
            int? teamId = null,
            DateTime? birthDate = null)
        {
            return (await _context.AddAsync(new User(id, firstName, lastName, email, birthDate ?? DateTime.Now.AddYears(40), teamId))).Entity;
        }

        public static async Task<Team> AddTeam(this ProjectDbContext _context,
            int id = 0,
            string name = "TEAM",
            DateTime? createdAt = null,
            IEnumerable<User> members = null)
        {
            return (await _context.AddAsync(new Team(id, name, createdAt ?? DateTime.Now) { Members = members })).Entity;
        }

        public static async Task<Task> AddTask(this ProjectDbContext _context,
            int performerId,
            int projectId,
            int id = 0,
            string name = "TaskName",
            string description = "Description",
            DateTime? createdAt = null,
            DateTime? finishedAt = null)
        {
            return (await _context.AddAsync(new Task(id, name, performerId, projectId, description, createdAt ?? DateTime.Now, finishedAt))).Entity;
        }

        public static async Task<Project> AddProject(this ProjectDbContext _context,
            int authorId,
            int teamId,
            int id = 0,
            string name = "ProjectName",
            string description = "ProjectDescription",
            DateTime? deadline = null,
            DateTime? createdAt = null
            )
        {
            return (await _context.AddAsync(new Project(id, name, description, authorId, teamId, deadline ?? DateTime.Now.AddMonths(3), createdAt ?? DateTime.Now))).Entity;
        }
    }
}
