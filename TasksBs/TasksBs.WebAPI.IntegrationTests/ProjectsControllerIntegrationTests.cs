﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using TasksBs.Common.DTOs;
using TasksBs.Common.DTOs.Project;
using TasksBs.Common.DTOs.User;
using TasksBs.DAL.Context;
using TasksBs.TestsCommon;
using Xunit;

namespace TasksBs.WebAPI.IntegrationTests
{
    public class ProjectsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        private readonly CustomWebApplicationFactory<Startup> _factory;
        public ProjectsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Fact]
        public async Task CreateProject_ReturnsCreatedProjectWithSuccessStatusCode()
        {
            TeamDto team;
            UserDto user;
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<ProjectDbContext>();
                var teamEntity = await context.AddTeam();
                var userEntity = await context.AddUser(teamId: teamEntity.Id);
                await context.SaveChangesAsync();
                var mapper = scope.ServiceProvider.GetRequiredService<IMapper>();
                team = mapper.Map<TeamDto>(teamEntity);
                user = mapper.Map<UserDto>(userEntity);
            }

            var project = DtoCreateTools.CreateProject(
                user,
                team
            );

            var response = await _client.PostAsJsonAsync("api/projects", project);
            response.EnsureSuccessStatusCode();

            var projectAnswer = await response.Content.ReadFromJsonAsync<ProjectDto>();
            Assert.Equal(project.Name, projectAnswer.Name);
            Assert.Equal(project.Description, projectAnswer.Description);
            Assert.Equal(project.Deadline, projectAnswer.Deadline);
            Assert.Equal(project.Author.FirstName, projectAnswer.Author.FirstName);
            Assert.Equal(project.Team.Name, projectAnswer.Team.Name);
            Assert.NotEqual(DateTime.MinValue, projectAnswer.CreatedAt);
        }

        [Fact]
        public async Task CreateProject_BadBody_ReturnsBadRequestCode()
        {
            var response = await _client.PostAsJsonAsync("api/projects", "someDataDSASDFAs");

            Assert.Equal(System.Net.HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
