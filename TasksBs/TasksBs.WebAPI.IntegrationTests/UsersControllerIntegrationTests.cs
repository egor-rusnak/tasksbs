﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;
using TasksBs.DAL.Context;
using TasksBs.TestsCommon;
using Xunit;

namespace TasksBs.WebAPI.IntegrationTests
{
    public class UsersControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private CustomWebApplicationFactory<Startup> _factory;
        private readonly HttpClient _client;
        public UsersControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = _factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Fact]
        public async System.Threading.Tasks.Task RemoveUser_UserRemovedwithNoContentCode()
        {
            int idToRemove = 0;
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<ProjectDbContext>();
                var entity = await context.AddUser();
                await context.SaveChangesAsync();
                idToRemove = entity.Id;
            }

            var response = await _client.DeleteAsync($"api/users/{idToRemove}");

            response.EnsureSuccessStatusCode();
            Assert.Equal(System.Net.HttpStatusCode.NoContent, response.StatusCode);
        }

        [Fact]
        public async System.Threading.Tasks.Task RemoveUser_WhenNotExist_NotFoundCodeReturned()
        {
            int idToRemove = 0;
            using (var scope = _factory.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<ProjectDbContext>();
                var entity = await context.AddUser();
                await context.SaveChangesAsync();
                idToRemove = entity.Id;
            }

            idToRemove++;
            var response = await _client.DeleteAsync($"api/users/{idToRemove}");

            Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}
