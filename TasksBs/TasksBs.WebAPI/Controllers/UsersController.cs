﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TasksBs.BLL.Interfaces;
using TasksBs.Common.DTOs.Project;
using TasksBs.Common.DTOs.User;

namespace TasksBs.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDto>>> GetAll()
        {
            return Ok(await _userService.GetAllUsers());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDto>> Get(int id)
        {
            try
            {
                var user = await _userService.GetById(id);
                return Ok(user);
            }
            catch (ArgumentException ex) { return NotFound(ex.Message); }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDto>> CreateUser([FromBody] UserDto user)
        {
            if (!ModelState.IsValid)
                return BadRequest("Bad data");

            try
            {
                var result = await _userService.AddUser(user);
                return CreatedAtAction(nameof(Get), new
                {
                    id = result.Id
                }, result);
            }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpPut]
        public async Task<ActionResult<ProjectDto>> UpdateUser([FromBody] UserDto user)
        {
            if (!ModelState.IsValid)
                return BadRequest("Bad data");

            try
            {
                var result = await _userService.UpdateUser(user);
                return Ok(result);
            }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUserAsync(int id)
        {
            try
            {
                await _userService.RemoveUser(id);
                return NoContent();
            }
            catch (ArgumentException ex) { return NotFound(ex.Message); }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }        }

    }
}
