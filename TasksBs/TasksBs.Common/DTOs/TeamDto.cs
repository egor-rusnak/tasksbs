﻿using System;
using TasksBs.Common.DTOs.Abstraction;

namespace TasksBs.Common.DTOs
{
    public class TeamDto : BaseEntityDto
    {
        public DateTime CreatedAt { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Id + "-" + Name;
        }
    }
}
