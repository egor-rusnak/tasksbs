﻿using System;
using TasksBs.Common.DTOs.Abstraction;
using TasksBs.Common.DTOs.Project;
using TasksBs.Common.DTOs.User;

namespace TasksBs.Common.DTOs
{
    public class TaskDto : BaseEntityDto
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
        public ProjectDto Project { get; set; }
        public string Description { get; set; }
        public UserDto Performer { get; set; }

        public override string ToString()
        {
            return Id + "-" + Name;
        }
    }
}
