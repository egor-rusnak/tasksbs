﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TasksBs.BLL.Interfaces;
using TasksBs.BLL.Services.Abstraction;
using TasksBs.Common.DTOs;
using TasksBs.DAL.Context;
using Task = TasksBs.DAL.Entities.Task;

namespace TasksBs.BLL.Services
{
    public class TaskService : BaseSerivce, ITaskService
    {
        public TaskService(ProjectDbContext context, IMapper mapper)
            : base(context, mapper) { }

        public async Task<TaskDto> AddTask(TaskDto task)
        {
            var taskEntity = _mapper.Map<Task>(task);

            await _context.Tasks.AddAsync(taskEntity);

            await _context.SaveChangesAsync();
            return _mapper.Map<TaskDto>(await GetTask(taskEntity.Id));
        }

        public async Task<TaskDto> UpdateTask(TaskDto task)
        {
            var taskEntity = _mapper.Map<Task>(task);

            _context.Tasks.Update(taskEntity);

            await _context.SaveChangesAsync();
            return _mapper.Map<TaskDto>(await GetTask(taskEntity.Id));
        }

        public async System.Threading.Tasks.Task RemoveTask(int id)
        {
            var taskEntity = await GetTask(id);

            if (taskEntity == null)
                throw new ArgumentException("No entity with this id!");

            _context.Tasks.Remove(taskEntity);

            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<TaskDto>> GetAllTasks()
        {
            return _mapper.Map<IEnumerable<TaskDto>>(await GetTasks().ToListAsync());
        }

        public async Task<TaskDto> GetById(int id)
        {
            var taskEntity = await GetTask(id);

            if (taskEntity == null)
                throw new ArgumentException("No entity with this id");

            return _mapper.Map<TaskDto>(taskEntity);
        }

        public async System.Threading.Tasks.Task SetTaskAsFinished(int taskId)
        {
            var task = await GetTask(taskId);
            if (task == null)
                throw new ArgumentException("No task with this id");

            task.FinishedAt = DateTime.Now;

            _context.Tasks.Update(task);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<TaskDto>> GetUnfinishedTasksByUser(int userId)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);
            if (user == null)
                throw new ArgumentException("No user with this id");

            var tasks = await GetTasks().Where(t => t.PerformerId == user.Id && t.FinishedAt == null).ToListAsync();

            return _mapper.Map<IEnumerable<TaskDto>>(tasks);
        }

        private IQueryable<Task> GetTasks()
        {
            return _context.Tasks.Include(t => t.Performer).Include(t => t.Project);
        }
        private async Task<Task> GetTask(int id)
        {
            return await GetTasks().FirstOrDefaultAsync(t => t.Id == id);
        }

    }
}
