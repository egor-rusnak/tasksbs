﻿using AutoMapper;
using TasksBs.Common.DTOs;
using TasksBs.DAL.Entities;

namespace TasksBs.BLL.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDto>();
            CreateMap<TeamDto, Team>();
        }
    }
}
