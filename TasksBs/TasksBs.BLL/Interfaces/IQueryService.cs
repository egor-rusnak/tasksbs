﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TasksBs.Common.DTOs;
using TasksBs.Common.DTOs.Project;
using TasksBs.Common.DTOs.User;

namespace TasksBs.BLL.Interfaces
{
    public interface IQueryService
    {
        Task<IEnumerable<KeyValuePair<UserDto, IEnumerable<TaskDto>>>> ExecuteQueryFive();
        Task<IEnumerable<KeyValuePair<KeyValuePair<int, string>, IEnumerable<UserDto>>>> ExecuteQueryFour();
        Task<Dictionary<ProjectDto, int>> ExecuteQueryOne(int userId);
        Task<IEnumerable<ProjectInfoDto>> ExecuteQuerySeven();
        Task<UserInfoDto> ExecuteQuerySix(int userId);
        Task<IEnumerable<KeyValuePair<int, string>>> ExecuteQueryThree(int userId);
        Task<IEnumerable<TaskDto>> ExecuteQueryTwo(int userId);
    }
}