﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TasksBs.Common.DTOs.User;

namespace TasksBs.BLL.Interfaces
{
    public interface IUserService
    {
        Task<UserDto> AddUser(UserDto user);
        Task<IEnumerable<UserDto>> GetAllUsers();
        Task<UserDto> GetById(int id);
        Task RemoveUser(int id);
        Task<UserDto> UpdateUser(UserDto user);
    }
}