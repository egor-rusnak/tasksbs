﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TasksBs.Common.DTOs.Abstraction;

namespace TasksBs.UI.Model.Interfaces
{
    public interface ICRUDService<T> where T : BaseEntityDto
    {
        Task<T> Create(T entity);
        Task<T> Update(T entity);
        Task<IEnumerable<T>> GetAll();
        Task Delete(int id);
    }
}
