﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using TasksBs.Common.DTOs.Abstraction;
using TasksBs.UI.Extensions;
using TasksBs.UI.Model.Interfaces;
namespace TasksBs.UI.Model.Services
{
    public class ApiCRUDService<T> : ICRUDService<T> where T : BaseEntityDto
    {

        private string _prefixApiString;

        private readonly HttpClient _client;
        public ApiCRUDService(Uri apiAdress, string prefixApiString)
        {
            _client = new HttpClient()
            {
                BaseAddress = apiAdress
            };
            _prefixApiString = prefixApiString;
        }

        public async Task<T> Create(T entity)
        {
            var resultEntity = await _client.HandlePostRequest<T, T>(_prefixApiString, entity);
            return resultEntity;
        }

        public async Task<T> Update(T entity)
        {
            var resultEntity = await _client.HandlePutRequest<T, T>(_prefixApiString, entity);

            return resultEntity;
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await _client.HandleGetRequest<List<T>>(_prefixApiString);
        }

        public async Task Delete(int id)
        {
            await _client.HandleDeleteRequest(_prefixApiString + id);
        }
    }
}
