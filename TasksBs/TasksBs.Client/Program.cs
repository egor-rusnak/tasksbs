﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using TasksBs.Common.DTOs;
using TasksBs.Common.DTOs.Project;
using TasksBs.Common.DTOs.User;
using TasksBs.UI.Commands;
using TasksBs.UI.Commands.WorkWithData;
using TasksBs.UI.Model.Interfaces;
using TasksBs.UI.Model.Services;

namespace TasksBs
{
    class Program
    {
        private static List<ICommand> commands = new List<ICommand>();

        public static event EventHandler CancelCompletion;
        static async Task Main(string[] args)
        {
            CultureInfo.CurrentCulture = CultureInfo.GetCultureInfo("ua");

            var uri = new Uri("https://localhost:44380/");

            var queryService = new ApiQueryService(uri);
            var projectCrudService = new ApiCRUDService<ProjectDto>(uri, "api/Projects/");
            var taskCrudService = new ApiCRUDService<TaskDto>(uri, "api/Tasks/");
            var userCrudService = new ApiCRUDService<UserDto>(uri, "api/Users/");
            var teamCrudService = new ApiCRUDService<TeamDto>(uri, "api/Teams/");


            SetCommands(queryService, projectCrudService, taskCrudService, teamCrudService, userCrudService);
            await RunApplication();
        }

        static async Task RunApplication()
        {
            Console.WriteLine("Console interface for BS task `Tasks`");
            Console.WriteLine("To show menu, write a 0");
            ShowMenu();
            while (true)
            {
                Console.Write("Input a command (0-show menu): ");
                var input = Console.ReadLine();

                if (int.TryParse(input, out int command))
                {
                    if (command > 0 && command <= commands.Count)
                    {
                        try
                        {
                            await commands[command - 1].Execute();
                            Console.WriteLine();
                            continue;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Unhandled error while executing command: " + ex.Message);
                            continue;
                        }
                    }
                    else if (command == 0)
                    {
                        ShowMenu();
                        continue;
                    }
                    else if (command == commands.Count + 1) break;
                }

                Console.WriteLine("Wrong Input");
            }
            Console.WriteLine("Good Bye!");
        }

        static void ShowMenu()
        {
            Console.WriteLine("0-Show menu");
            int index = 1;
            foreach (var command in commands)
            {
                Console.WriteLine(index + "-" + command.Description);
                index++;
            }
            Console.WriteLine((index) + "-Exit");
        }

        private static void SetCommands(
            IQueryService queries,
            ICRUDService<ProjectDto> projectCrud,
            ICRUDService<TaskDto> tasksCrud,
            ICRUDService<TeamDto> teamsCrud,
            ICRUDService<UserDto> usersCrud)
        {
            commands.Add(new ExecuteFirstQuery(queries));
            commands.Add(new ExecuteSecondQuery(queries));
            commands.Add(new ExecuteThirdQuery(queries));
            commands.Add(new ExecuteFourthQuery(queries));
            commands.Add(new ExecuteFifthQuery(queries));
            commands.Add(new ExecuteSixthQuery(queries));
            commands.Add(new ExecuteSeventhQuery(queries));
            commands.Add(new WorkWithProjectsCRUD(projectCrud, usersCrud, teamsCrud));
            commands.Add(new WorkWithTasksCRUD(tasksCrud, projectCrud, usersCrud));
            commands.Add(new WorkWithTeamsCRUD(teamsCrud));
            commands.Add(new WorkWithUsersCRUD(usersCrud, teamsCrud));
            commands.Add(new StartTaskCompletion(queries, ref CancelCompletion));
            commands.Add(new CancelTaskCompletion(CancelCompletion));
        }
    }
}
