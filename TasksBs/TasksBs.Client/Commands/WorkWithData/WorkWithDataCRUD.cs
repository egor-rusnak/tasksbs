﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TasksBs.Common.DTOs.Abstraction;
using TasksBs.UI.Model.Interfaces;
using static TasksBs.UI.Tools.InputWorker;
namespace TasksBs.UI.Commands
{
    public abstract class WorkWithDataCRUD<T> : ICommand where T : BaseEntityDto
    {

        public string Description { get; set; }

        protected readonly ICRUDService<T> _baseService;
        public WorkWithDataCRUD(ICRUDService<T> service)
        {
            _baseService = service;
        }

        public async Task Execute()
        {
            Console.WriteLine(Description);
            Console.WriteLine("1-Create");
            Console.WriteLine("2-Update");
            Console.WriteLine("3-Read");
            Console.WriteLine("4-Delete");
            Console.WriteLine("5-Exit");
            while (true)
            {
                Console.Write("Input:");
                var result = Console.ReadLine();
                if (result == "1")
                    await Create();
                else if (result == "2")
                    await Update();
                else if (result == "3")
                    await Read();
                else if (result == "4")
                    await Delete();
                else if (result == "5")
                    break;
            }
        }

        public async Task Create()
        {
            var entity = await CreateEntity();

            var result = await _baseService.Create(entity);
            if (result != null)
                Console.WriteLine("=====\n" + result.ToString() + " was created..\n=====");
            else
                Console.WriteLine("Error creating entity!");
        }

        public async Task Delete()
        {
            try
            {
                await _baseService.Delete(GetNumber("Input entity id to delete: "));
                Console.WriteLine("Success!");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }

        public async Task Read()
        {
            var entities = await _baseService.GetAll();
            Console.WriteLine("List:");
            foreach (var entity in entities)
            {
                Console.WriteLine(entity.ToString());
            }
        }

        public async Task Update()
        {
            int id = GetNumber("Input id to update: ");

            var entity = (await _baseService.GetAll()).FirstOrDefault(e => e.Id == id);
            if (entity == null)
            {
                Console.WriteLine("No entities with this id");
                return;
            }

            var resultEntity = await UpdateEntity(entity);

            try
            {
                await _baseService.Update(resultEntity);
                Console.WriteLine("Success!");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }

        public abstract Task<T> CreateEntity();
        public abstract Task<T> UpdateEntity(T entity);

    }
}
