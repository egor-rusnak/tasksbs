﻿using System;
using System.Threading.Tasks;
using TasksBs.Common.DTOs;
using TasksBs.Common.DTOs.Project;
using TasksBs.Common.DTOs.User;
using TasksBs.UI.Model.Interfaces;
using static TasksBs.UI.Tools.InputWorker;

namespace TasksBs.UI.Commands.WorkWithData
{
    public class WorkWithTasksCRUD : WorkWithDataCRUD<TaskDto>
    {
        private readonly ICRUDService<UserDto> _userService;
        private readonly ICRUDService<ProjectDto> _projectService;
        public WorkWithTasksCRUD(ICRUDService<TaskDto> service, ICRUDService<ProjectDto> projectService, ICRUDService<UserDto> userService) : base(service)
        {
            Description = "Tasks";
            _projectService = projectService;
            _userService = userService;
        }

        public override async Task<TaskDto> CreateEntity()
        {
            var task = new TaskDto()
            {
                CreatedAt = DateTime.Now,
                Name = GetString("Enter task name: "),
                Description = GetString("Enter task Description: "),
                FinishedAt = GetDate("Enter Finished At date: ", true),
                Performer = await Input(_userService, "Choose performer id:"),
                Project = await Input(_projectService, "Choose project id:")
            };
            return task;
        }

        public override async Task<TaskDto> UpdateEntity(TaskDto entity)
        {
            entity.Name = GetString($"Enter new task name (old: {entity.Name}): ");
            entity.Description = GetString($"Enter new task Description (old: {entity.Description}): ");
            entity.FinishedAt = GetDate($"Enter new FinishedAt date (old: {entity.FinishedAt}): ", true);
            entity.Performer = await Input(_userService, $"Input a new performer id (old: {entity.Performer}): ");
            entity.Project = await Input(_projectService, $"Input new project id (old: {entity.Project}): ");

            return entity;
        }
    }
}
