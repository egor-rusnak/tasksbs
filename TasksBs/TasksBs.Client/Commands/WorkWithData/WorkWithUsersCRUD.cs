﻿using System;
using System.Threading.Tasks;
using TasksBs.Common.DTOs;
using TasksBs.Common.DTOs.User;
using TasksBs.UI.Model.Interfaces;
using static TasksBs.UI.Tools.InputWorker;

namespace TasksBs.UI.Commands.WorkWithData
{
    public class WorkWithUsersCRUD : WorkWithDataCRUD<UserDto>
    {
        private readonly ICRUDService<TeamDto> _teamService;

        public WorkWithUsersCRUD(ICRUDService<UserDto> service, ICRUDService<TeamDto> teamservice) : base(service)
        {
            _teamService = teamservice;
            Description = "Users";
        }

        public override async Task<UserDto> CreateEntity()
        {
            var user = new UserDto()
            {
                BirthDay = GetDate("Enter user BirthDay date: ").Value,
                FirstName = GetString("Enter user first name: "),
                LastName = GetString("Enter user last name: "),
                Email = GetString("Enter user email: "),
                RegisteredAt = DateTime.Now,
                Team = await Input(_teamService, "Choose team id (0 if null): ", true)
            };
            return user;
        }

        public override async Task<UserDto> UpdateEntity(UserDto entity)
        {
            entity.BirthDay = GetDate($"Enter new user BirthDay date (old: {entity.BirthDay}): ").Value;
            entity.FirstName = GetString($"Enter new user first name (old: {entity.FirstName}): ");
            entity.LastName = GetString($"Enter new user last name (old: {entity.LastName}): ");
            entity.Email = GetString($"Enter new user email (old: {entity.Email}): ");
            entity.Team = await Input(_teamService, $"Choose team (old: {entity.Team}): ", true);
            return entity;
        }
    }
}
