﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TasksBs.Common.DTOs.Abstraction;
using TasksBs.UI.Model.Interfaces;

namespace TasksBs.UI.Tools
{
    public static class InputWorker
    {
        public static string GetString(string title)
        {
            Console.Write(title);
            var result = Console.ReadLine();
            if (result == "exit")
                throw new ArgumentException("Not written data");
            return result;
        }

        public static int GetNumber(string title)
        {
            Console.Write(title);
            while (true)
            {
                var result = Console.ReadLine();
                if (result == "exit")
                    throw new ArgumentException("Not written data");

                if (int.TryParse(result, out int number))
                    return number;
            }
        }

        public static DateTime? GetDate(string title, bool nullable = false)
        {
            Console.WriteLine("To exit-exit " + (nullable ? " for null - null" : ""));
            Console.Write(title);
            while (true)
            {
                var result = Console.ReadLine();
                if (result == "exit")
                    throw new ArgumentException("Not written data");

                if (DateTime.TryParse(result, out DateTime date))
                    return date;

                if (result == "null" && nullable)
                    return null;
            }

        }

        public static async Task<T> Input<T>(ICRUDService<T> service, string title = "Choose id:", bool nullable = false) where T : BaseEntityDto
        {
            var entities = await service.GetAll();

            Console.WriteLine("List:");
            foreach (var entity in entities)
            {
                Console.WriteLine(entity);
            }

            int result = GetNumber(title);

            return entities.FirstOrDefault(e => e.Id == result) ?? (nullable ? null : throw new ArgumentException("No data with this id!"));
        }

    }
}
