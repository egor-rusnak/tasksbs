﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace TasksBs.UI.Extensions
{
    public static class HttpClientExtensions
    {
        public static async Task<T> HandleGetRequest<T>(this HttpClient client, string path)
        {
            var response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
                return await response.Content.ReadFromJsonAsync<T>();
            else
                throw new Exception(await response.Content.ReadAsStringAsync() + "\n" + response.ReasonPhrase);
        }

        public static async Task<U> HandlePostRequest<T, U>(this HttpClient client, string path, T data)
        {
            var response = await client.PostAsJsonAsync(path, data);
            if (response.IsSuccessStatusCode)
                return await response.Content.ReadFromJsonAsync<U>();
            else
                throw new Exception(await response.Content.ReadAsStringAsync() + "\n" + response.ReasonPhrase);
        }

        public static async Task HandlePatchRequest(this HttpClient client, string path)
        {
            var response = await client.PatchAsync(path, null);

            if (!response.IsSuccessStatusCode)
                throw new Exception(await response.Content.ReadAsStringAsync() + "\n" + response.ReasonPhrase);
        }

        public static async Task HandleDeleteRequest(this HttpClient client, string path)
        {
            var response = await client.DeleteAsync(path);

            if (!response.IsSuccessStatusCode)
                throw new Exception(await response.Content.ReadAsStringAsync() + "\n" + response.ReasonPhrase);
        }

        public static async Task<U> HandlePutRequest<T, U>(this HttpClient client, string path, T entity)
        {
            var response = await client.PutAsJsonAsync(path, entity);

            if (response.IsSuccessStatusCode)
                return await response.Content.ReadFromJsonAsync<U>();
            else
                throw new Exception(await response.Content.ReadAsStringAsync() + "\n" + response.ReasonPhrase);
        }
    }
}
